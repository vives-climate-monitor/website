#!/bin/sh

mkdir /run/nginx/

(nginx || kill -SIGHUP $PPID) &

php-fpm -F -O &

trap "exit 0" INT

echo 'Nginx and php-fpm running... Press <ctrl-c> to exit.'

wait
