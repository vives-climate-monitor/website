
  

# Project ICT - Climate control

  

> Vue, Laravel, PHP, MySQL, Chart.js

  

## About Project

  

- Built as a college assignment

- built with Laravel & Vue.js

- Chart.js

- Redis caching
  

## Installation

  

### prerequisites

- access to an SQL database

- access to a Redis instance (optional)

- copy .env.example => .env

	- configure as required

 #### - meeting prerequisites with Docker && docker-compose for example
 ```
 version: '3.1'
services:
  db:
    image: mysql
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    volumes:
      - ./myqsl/data:/var/lib/mysql
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: password

  admin:
    image: phpmyadmin/phpmyadmin
    container_name: admin
    restart: always
    ports:
      - 8181:80
    environment:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: password
    depends_on:
      - db

  redis:
    container_name: redis
    image: redis
    ports:
      - "6379:6379"
    volumes:
      - ./redis:/data
    entrypoint: redis-server --appendonly yes
    restart: always
 ```

### Development

#### - .env configuration
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=password

#if using redis

CACHE_DRIVER=redis
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
```
#### -  installing and running the project
```
--php--

1) composer install

2) php artisan migrate

3) php artisan key:generate

4) php artisan serve

--npm--

1) npm i

2) npm run watch

```

  

### building a 'production' Docker-image

```

docker build -t name/image-name .

```

  

## Common errors

- 'requires' ext-dom

- Fixed by running 'sudo apt install php-xml php-mbstring'

## Usefull tools
- Valet (serving the project with domainname, https & ssl)
	- Windows: [https://github.com/cretueusebiu/valet-windows](https://github.com/cretueusebiu/valet-windows "https://github.com/cretueusebiu/valet-windows")
	- Linux : [https://github.com/cpriego/valet-linux](https://github.com/cpriego/valet-linux) 
	- Mac: [https://laravel.com/docs/7.x/valet](https://laravel.com/docs/7.x/valet)