<?php



return [
    /*
    |--------------------------------------------------------------------------
    | Meting Constants
    |--------------------------------------------------------------------------
    |
    | This option controls the default MAX & MIN for the properties
    | of the Meting:class
    |
    */

    'meting' => [
        'co2_max' => 750,
        'humidity_min' => 40,
        'humidity_max' => 70,
        'temperature_min' => 17,
        'temperature_max' => 25
    ]
];
