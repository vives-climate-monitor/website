<?php

use App\Http\Controllers\API\MetingAverageController;
use App\Http\Controllers\API\MetingController;
use App\Http\Controllers\API\TimeAboveController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RoomController;

Route::middleware('auth:api')->get('/user', 'UserController@AuthRouteAPI');

Route::post('/meting', [MetingController::class, 'store']);

Route::get('/rooms', [RoomController::class, 'list']);
Route::post('/time-above', [TimeAboveController::class, 'invoke']);
Route::post('/average', [MetingAverageController::class, 'invoke']);
