<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/', 'HomeController@index')->name('home');

/* Metingen */
Route::get('/rooms', 'MetingController@index')->name('metings.rooms');
Route::get('/metings/list', 'MetingController@list')->name('metings.list');
Route::get('/rooms/{room}', 'MetingController@room')->name('metings.room');
Route::post('/rooms/{room}', 'MetingController@room');
Route::get('/average', 'MetingController@average')->name('metings.average');
Route::resource('metings', 'MetingController');

/* Language */
Route::get('/language/{language}', 'UserController@setLanguage');
