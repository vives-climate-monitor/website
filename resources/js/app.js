/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import BootstrapVue from "bootstrap-vue"; //Importing
window.Vue = require("vue");

Vue.use(BootstrapVue);

/* languages */

import VueI18n from "vue-i18n";

import messages from "./messages";

const i18n = new VueI18n({
    locale: "nl", // set locale
    messages // set locale messages
});

Vue.component(
    "line-chart-component",
    require("./components/LineChart.vue").default
);
Vue.component(
    "bar-chart-component",
    require("./components/BarChart.vue").default
);
Vue.component(
    "average-line-chart-component",
    require("./components/AverageLineChart.vue").default
);

Vue.mixin({
    methods: {
        capitalize: function(str, lower = false) {
            return (lower
                ? str.toLowerCase()
                : str
            ).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
        }
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    i18n,
    el: "#app"
});
