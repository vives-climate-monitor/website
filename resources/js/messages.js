export default {
    en: {
        temperature: 'temperature',
        Temperature: 'Temperature',
        humidity: 'humidity',
        Humidity: 'Humidity',
        co2: 'co2',
        Co2: 'Co2',
        average: 'Average',
        Realtime: 'per 5 minutes',
        Hourly: 'per hour',
        Daily: 'per day',
        Weekly: 'per week',
        Monthly: 'per month',
        Room: 'no rooms selected | 1 room selected | {count} rooms selected',
        Period: 'Period',
        BarChartTitle : 'Hours above certain value'
    },
    nl: {
        temperature: 'temperatuur',
        Temperature: 'Temperatuur',
        humidity: 'vochtigheid',
        Humidity: 'Vochtigheid',
        co2: 'co2',
        Co2: 'Co2',
        average: 'Gemiddelde',
        Realtime: 'per 5 minuten',
        Hourly: 'per uur',
        Daily: 'per dag',
        Weekly: 'per week',
        Monthly: 'per maand',
        Room: 'geen lokalen geselecteerd | 1 lokaal geselecteerd | {count} lokalen geselecteerd',
        Period: 'Periode',
        BarChartTitle : 'Aantal uur boven bepaalde waarde'
    }
}
