@extends('layouts.app')
@section('content')
<div class="container text-center">
    <h2>Meting #{{ $meting->id }}</h2>
    <table class="table table-show">
        <tbody>
            <tr>
                <th>{{ __('room') }}</th>
                <td>{{ $meting->room }}</td>
            </tr>
            <tr>
                <th>Co2</th>
                <td>{{ $meting->co2 }}</td>
            </tr>
            <tr>
                <th>{{ __('humidity') }}</th>
                <td>{{ $meting->humidity }}</td>
            </tr>
            <tr>
                <th>{{ __('timestamp')}}</th>
                <td>{{ $meting->created_at }}</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="mt-5 container text-center">
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('metings.list') }}"> {{ __('back') }}</a>
    </div>
</div>
@endsection