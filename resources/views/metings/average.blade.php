@extends('layouts.app')
@section('content')
<div class="text-center">
    <div class="mx-auto">
        <h2>{{ __('average') }}</h2>
        <x-meting-table :meting="$meting"></x-meting-table>
        <average-line-chart-component locale="{{ app()->getLocale() }}"></average-line-chart-component>
    </div>
</div>
@endsection
