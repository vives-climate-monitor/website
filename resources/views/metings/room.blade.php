@extends('layouts.app')
@section('content')
    <div class="text-center">
        <div>
            @if ($meting->time_since_last > 5)
                <div class="col-sm-12">
                    <div style="max-width: 400px" class="mx-auto alert alert-danger alert-dismissible fade show" role="alert">
                       {{ __('sensorOffline', ['minutes' => $meting->time_since_last])}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
            <h2> {{ __('room')}} {{ $meting->room }}</h2>
                <x-meting-table :meting="$meting"></x-meting-table>
                <div id="app">
                    <line-chart-component room="{{ $meting->room }}" locale="{{ app()->getLocale() }}"></line-chart-component>
                    <bar-chart-component room="{{ $meting->room }}"
                                         :timedata='@json($timeAboveData)'></bar-chart-component>
                </div>
        </div>
        @if (!empty($count))
            <div class="mt-5">
                {{ __('numberOfRecords') }} {{ $count }}
            </div>
        @endif
    </div>
@endsection
