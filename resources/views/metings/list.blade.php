@extends('layouts.app')

@section('content')
<div class="container">
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <table class="table table-striped" style="max-width: 100%; font-size: 12px;">
        <thead class="thead-dark">
            <tr>
                <th>No</th>
                <th>id</th>
                <th>{{ __('room') }}</th>
                <th>Co2</th>
                <th>{{ __('humidity') }}</th>
                <th>{{ __('temperature') }}</th>
                <th>{{ __('createdAt') }}</th>
                <th width="215px">{{ __('action') }}</th>
            </tr>
        </thead>
        <tbody style="font-size: 18px">
            @foreach ($metings as $meting)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $meting->id }}</td>
                <td>{{ $meting->room }}</td>
                <td>{{ $meting->co2 }}</td>
                <td>{{ $meting->humidity }}</td>
                <td>{{ $meting->temperature }}</td>
                <td>{{ $meting->created_at }}</td>
                <td>
                    <form action="{{ route('metings.destroy', $meting->id ) }}" method="POST">

                        <a class="btn btn-primary" href="{{ route('metings.show', $meting->id ) }}">{{ __('show') }}</a>

                        <a class="btn btn-success" href="{{ route('metings.edit', $meting->id ) }}">{{ __('edit') }}</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $metings->onEachSide(1)->links() !!}
</div>
@endsection
