@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card-row">
        @forelse ($metings as $meting)
        <a href="{{ route('metings.room', $meting->room) }}" @if (!$meting->temperature_good || !$meting->humidity_good || !$meting->co2_good)
            data-html="true" data-toggle="tooltip" data-placement="top" title="{{ __('badValue') }}"
            @endif
            >
            <div class="col">
                <div class="card rounded overflow-hidden shadow-sm">
                    <div class="card-inner">
                        <div class="card-title">{{ __('room') }} {{ $meting->room }}
                            @if (!$meting->temperature_good || !$meting->humidity_good || !$meting->co2_good)
                            <span id="room-status-icon" style="float:right;" class="badge badge-pill">!</span>
                            @endif
                        </div>
                        <div class="card-value">CO&#8322;: {{ $meting->co2 }} ppm</div>
                        <div class="card-value">{{ __('humidity') }}: {{ $meting->humidity }} %</div>
                        <div class="card-value">{{ __('temperature') }}: {{ $meting->temperature }} °C</div>
                        <div class="card-value">{{ __('timestamp') }}: {{ $meting->created_at }}</div>
                    </div>
                </div>
            </div>
        </a>
        @empty
        <div>
            <div colspan="6">{{ __('noRecordsFound') }}</div>
        </div>
        @endforelse
    </div>
    <div class="container mt-3 justify-content-center">
        {{ $metings->links() }}
    </div>

    <div class="mt-5 text-center container">
        {{ __('numberOfRecords') }} {{ $count }}
    </div>
</div>
@endsection