@extends('layouts.app')

@section('content')
<div class="row text-center">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>edit Meting {{ $meting->id }}</h2>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('metings.update', $meting->id ) }}" method="POST">
    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('room') }}:</strong>
                <input type="number" min="100" max="200" name="room" class="form-control" placeholder="room" value="{{ $meting->room }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Co2:</strong>
                <input type="number" min="0" max="2000" class="form-control" name="co2" placeholder="600" value="{{ $meting->co2 }}"></input>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('humidity') }}:</strong>
                <input type="number" min="0" max="100" class="form-control" name="humidity" placeholder="50" value="{{ $meting->humidity }}"></input>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('temperature') }}:</strong>
                <input type="number" min="10" max="40" class="form-control" name="temperature" placeholder="20" value="{{ $meting->temperature }}"></input>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success">{{ __('submit') }}</button>
        </div>
    </div>

</form>
<div class="mt-5 container text-center">
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('metings.list') }}"> {{ __('back') }}</a>
    </div>
</div>
@endsection