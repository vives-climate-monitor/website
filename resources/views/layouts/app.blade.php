<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('climateMonitoring') }}</title>

    <!-- Scripts -->
    <script src="{{ str_contains(env('APP_URL'), 'https') ? secure_asset('js/app.js') : asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/creativetimofficial/tailwind-starter-kit/compiled-tailwind.min.css" />
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ str_contains(env('APP_URL'), 'https') ? secure_asset('css/app.css') : asset('css/app.css')  }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ str_contains(env('APP_URL'), 'https') ? secure_asset('favicon.png') : asset('favicon.png')}}">

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>

<body @if (Route::current()->getName() == 'home') class="home-overflow-hidden" @endif >
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm sticky-top" style="padding: 3px 8px 3px 8px;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('images/vives-logo.png')}}" style="width: 40px; margin-right: 10px;"></img>
                    {{ __('climateMonitoring') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto text-center">
                        <!-- Authentication Links -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('metings.average') }}">{{ __('average') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('metings.rooms') }}">{{ __('rooms') }}</a>
                        </li>
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown" style="margin-top: -2px;">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <!-- Dashboard -->
                                <a class="dropdown-item" href="{{ route('dashboard') }}">
                                    Dashboard
                                </a>
                                <!-- logout -->
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>

                        </li>
                        @endguest
                        @if (App::isLocale('en'))
                        <li class="nav-btn-wrapper nav-item">
                            <a id="btnLan" class="vives-btn vives-btn-grey" href="{{ URL::action('UserController@setLanguage', 'nl') }}">NL</a>
                        </li>
                        @else
                        <li class="nav-btn-wrapper nav-item nav-btn">
                            <a id="btnLan" class="vives-btn vives-btn-grey" href="{{ URL::action('UserController@setLanguage', 'en') }}">EN</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @if (Route::current()->getName() == 'home')
        <div>
            @yield('home')
        </div>
        @else
        <main class="py-4">
            @yield('content')
        </main>
        @endif
    </div>
</body>

</html>
