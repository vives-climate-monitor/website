<div class="mx-auto mb-5 w-10/12 md:w-6/12">
    <div class="shadow overflow-hidden rounded border-gray-200">
        <table class="w-full bg-white text-center">
            <thead class="bg-red-600 text-white">
                <tr class="px-4">
                    <th class="py-3 px-4 uppercase font-semibold text-sm">
                        {{ __('property') }}
                    </th>
                    <th class="py-3 px-4 uppercase font-semibold text-sm">
                        {{ __('value') }}
                    </th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                <tr data-html="true" data-toggle="tooltip" title="{{ __('co2Warning', ["max" => config('constants.meting.co2_max')]) }}">
                    <th class="py-3 px-4">CO&#8322;</th>
                    <td style="position: relative" class="py-3 px-4">
                        <span>{{ number_format($meting->co2) }} ppm</span>
                        <span class="warning-icon">
                            @if (!$meting->co2_good )
                            <span style="float:right;" class="badge badge-pill">!</span>
                            @endif
                        </span>
                    </td>
                </tr>
                <tr class="bg-gray-100" data-html="true" data-toggle="tooltip" title="{{ __('humidityWarning', ["min" =>config('constants.meting.humidity_min'), "max" => config('constants.meting.humidity_max')])}}">
                    <th class="py-3 px-4">{{ __('humidity') }}</th>
                    <td style="position: relative" class="py-3 px-4">

                        <span>{{ number_format($meting->humidity) }} %</span>
                        <span class="warning-icon">
                            @if (!$meting->humidity_good )
                            <span style="float:right;" class="badge badge-pill">!</span>
                            @endif
                        </span>
                    </td>
                </tr>
                <tr data-html="true" data-toggle="tooltip" title="{{ __('temperatureWarning', ["min" => config('constants.meting.temperature_min'), "max" => config('constants.meting.temperature_max')])}}">
                    <th class="py-3 px-4">{{ __('temperature') }}</th>
                    <td style="position: relative" class="py-3 px-4">
                        <span>{{ number_format($meting->temperature) }} °C</span>
                        <span class="warning-icon">
                            @if (!$meting->temperature_good )
                            <span style="float:right;" class="badge badge-pill">!</span>
                            @endif
                        </span>
                    </td>
                </tr>
                <tr class="bg-gray-100">
                    <th class="py-3 px-4">{{ __('timestamp') }}</th>
                    <td class="py-3 px-4">{{ $meting->created_at }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>