@extends('layouts.app')

@section('content')
<div class="row container justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body text-center">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <p>
                    You are logged in!
                </p>
                <div class="mt-5 container text-center">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('metings.list') }}"> View Metingen</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection