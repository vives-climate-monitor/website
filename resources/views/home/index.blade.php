@extends('layouts.app')

@section('home')
<div id="homepage">
    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One -->
            <div class="slide-1-background carousel-item active">
                <div class="slide-1 text-center">
                    <h2 class="d-lg-none d-xl-none"><span>VIVES</span> {{ __('climateMonitoring') }}</h2>
                    <div style="padding: 20px 150px;" class="row justify-content-center container mt-5">
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 col-xl-6">
                            <h2 class="d-sm-none d-xs-none d-md-none d-none d-lg-block"><span>VIVES</span> {{ __('climateMonitoring') }}</h2>
                            <p>{{ __('slide1-Content') }}</p>
                            <p>#StaySafe #VeryVives #HealthyEnvironment</p>
                        </div>
                        <div class="slide-1-picture col-sm-12 col-xs-12 col-md-12 col-lg-3 col-xl-4">
                            <img alt="behuizing" src="/images/behuizing.png">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide Two -->
            <div class="carousel-item" style="background-color:hsla(0, 2.30%, 52.92%, 1.00)">
                <div class="slide-2 text-center">
                    <h2>{{ __('slide2-Title') }}</h2>
                    <div class="slide-2-picture  justify-content-center container mt-5">
                        <img style="max-width: 900px" class="img-rounded-shadow" alt="alle lokalen" src="/images/rooms.png">
                    </div>
                </div>
            </div>
            <!-- Slide Three -->
            <div class="carousel-item" style="background-color:hsla(37, 12.56%, 48.57%, 1.00)">
                <div class="slide-3 text-center">
                    <h2 class="d-lg-none d-xl-none">{{ __('slide3-Title') }}</h2>
                    <div style="padding: 0 220px;" class="row justify-content-center mt-5">
                        <div class="slide-3-picture col-sm-12 col-xs-12 col-md-12 col-lg-3 col-xl-4">
                            <img class="img-rounded-shadow" alt="Gemiddeld" src="/images/gemiddeld.png">
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-5 col-xl-6 ml-auto mt-md-4">
                            <h2 class="d-sm-none d-xs-none d-md-none d-none d-lg-block">{{ __('slide3-Title') }}</h2>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Slide Four -->
            <div class="carousel-item " style="background-color: rgb(95, 139, 108);">
                <div class="slide-4 text-center">
                    <h2 class="d-lg-none d-xl-none">{{ __('slide4-Title') }}</h2>
                    <div style="padding: 20px 220px;" class="row justify-content-center mt-5">
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-8 col-xl-6 mt-md-4">
                            <h2 class="d-sm-none d-xs-none d-md-none d-none d-lg-block">{{ __('slide4-Title') }}</h2>
                        </div>
                        <div class="slide-4-picture col-sm-12 col-xs-12 col-md-12 col-lg-3 col-xl-4 ml-auto mr-5">
                            <img class="img-rounded-shadow" alt="Lokaal Detail" src="/images/roomDetail.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
@endsection