<?php

use App\Meting;
use Faker\Generator as Faker;

$factory->define(Meting::class, function (Faker $faker) {
    return [
        'room' => mt_rand(0,10),
        'co2' => mt_rand(200, 1500),
        'temperature' => mt_rand(10, 40),
        'humidity' => mt_rand(0, 100),
        'tvoc' => mt_rand(0, 1800)
    ];
});
