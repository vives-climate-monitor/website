<?php

use App\Meting;
use Illuminate\Database\Seeder;

class MetingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Meting::class, 50)->create();
    }
}
