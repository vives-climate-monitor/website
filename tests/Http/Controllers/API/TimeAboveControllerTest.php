<?php

namespace Tests\Http\Controllers\API;

use App\Meting;
use Carbon\Carbon;
use Tests\TestCase;
use Tests\Traits\WithCache;
use Tests\Traits\WithDatabase;

class TimeAboveControllerTest extends TestCase
{
    use withDatabase;
    use WithCache;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testItCanGetTimeAboveForProperty()
    {
        $this->withoutExceptionHandling();

        create(Meting::class, [
            'room' => 5,
            'created_at' => Carbon::now(),
            'temperature' => 25,
        ]);

        create(Meting::class, [
            'room' => 5,
            'created_at' => Carbon::now()->subHours(2),
            'temperature' => 25,
        ]);

        $this->postJson('/api/time-above', [
            'room' => '5',
            'property' => 'temperature',
            'value' => 20,
        ])
            ->assertStatus(200)
            ->assertJsonPath('duration', 2);

        create(Meting::class, [
            'room' => 5,
            'created_at' => Carbon::now()->subHours(3),
            'temperature' => 25,
        ]);

        $this->clearCache();

        $this->postJson('/api/time-above', [
            'room' => '5',
            'property' => 'temperature',
            'value' => 20,
        ])
            ->assertStatus(200)
            ->assertJsonPath('duration', 3);

        create(Meting::class, [
            'room' => 5,
            'created_at' => Carbon::now()->subHours(4),
            'temperature' => 15,
        ]);

        $this->clearCache();

        $this->postJson('/api/time-above', [
            'room' => '5',
            'property' => 'temperature',
            'value' => 20,
        ])
            ->assertStatus(200)
            ->assertJsonPath('duration', 3);

        $this->clearCache();

        $this->postJson('/api/time-above', [
            'room' => '5',
            'property' => 'temperature',
            'value' => 15,
        ])
            ->assertStatus(200)
            ->assertJsonPath('duration', 4);
    }
}
