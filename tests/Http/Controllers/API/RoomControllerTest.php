<?php

namespace Tests\Http\Controllers\API;

use App\Meting;
use Tests\TestCase;
use Tests\Traits\WithCache;
use Tests\Traits\WithDatabase;

class RoomControllerTest extends TestCase
{
    use WithDatabase;
    use WithCache;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testItCanFetchRooms()
    {
        $this->getJson('/api/rooms')
            ->assertStatus(200)
            ->assertExactJson([]);

        create(Meting::class, [
            'room' => 5,
        ]);
        create(Meting::class, [
            'room' => 2,
        ]);

        $this->assertDatabaseCount('metings', 2);

        $this->clearCache();

        $this->getJson('/api/rooms')
            ->assertStatus(200)
            ->assertExactJson(['2', '5']);
    }
}
