<?php

namespace Tests\Http\Controllers\API;

use App\Meting;
use Carbon\Carbon;
use Tests\TestCase;
use Tests\Traits\WithCache;
use Tests\Traits\WithDatabase;

class MetingAverageControllerTest extends TestCase
{
    use withDatabase;
    use withCache;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testItCanFetchAverages()
    {
        create(
            Meting::class,
            [
                'room' => 5,
                'temperature' => 20,
                'created_at' => Carbon::now()->subHours(1),
            ]
        );
        create(
            Meting::class,
            [
                'room' => 6,
                'temperature' => 10,
                'created_at' => Carbon::now()->subHours(1),
            ]
        );

        $this->postJson(
            '/api/average',
            [
                'property' => 'Temperature',
                'rooms' => [],
                'interval' => 'Realtime',
                'period' => [null, null],
            ]
        )
            ->assertStatus(200)
            ->assertJsonPath('0.metingen.0.temperature', "15.0000")
            ->assertJsonPath('1.metingen.0.temperature', 10)
            ->assertJsonPath('2.metingen.0.temperature', 20);
    }
}
