<?php

namespace Tests\Http\Controllers\API;

use Tests\TestCase;
use Tests\Traits\WithDatabase;

class MetingControllerTest extends TestCase
{
    use WithDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testItCanCreateAMetingFromAPI()
    {
        $data = [
            'room' => 1,
            'co2' => 50,
            'temperature' => 40,
            'humidity' => 25,
            'tvoc' => 400,
        ];

        $this->postjson(
            '/api/meting',
            $data,
            [
                'authorization' => env('SECRET_KEY'),
            ]
        )
            ->assertSuccessful()
            ->assertStatus(201);

        $this->assertDatabaseCount('metings', 1);
        $this->assertDatabaseHas('metings', $data);
    }
}
