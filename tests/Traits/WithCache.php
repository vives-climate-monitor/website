<?php

namespace Tests\Traits;

use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\StreamOutput;

trait WithCache
{
    public function clearCache()
    {
        $output = new StreamOutput(fopen('php://stderr', 'w'));

        if (Artisan::call('cache:clear', ['-q' => true], $output)) {
            $this->fail();
        }
    }
}
