<?php

namespace Tests\Traits;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\StreamOutput;

trait WithDatabase
{
    protected function setUpDatabase()
    {
        $this->migrateTestDatabase();
    }

    private function migrateTestDatabase()
    {
        // Fail every test if the migration has failed
        // Without this code the test would continue without throwing any kind of error
        $output = new StreamOutput(fopen('php://stderr', 'w'));

        $skipFreshMigration = in_array('--skip-migration', $_SERVER['argv'], true);

        if ($skipFreshMigration) {
            if (Artisan::call('migrate:fresh', ['-q' => true], $output)) {
                $this->fail();
            }
        }

        DB::transaction(function () use ($output) {
            $command = 'migrate:fresh';

            if (Artisan::call($command, ['-q' => true], $output)) {
                $this->fail();
            }

            if (Artisan::call('db:seed', ['-q' => true], $output)) {
                $this->fail();
            }
        });

        $this->app[Kernel::class]->setArtisan(null);
    }
}
