<?php

namespace Tests\Unit;

use App\Meting;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MetingTest extends TestCase
{
    use RefreshDatabase;


    public function testCo2GoodAttribute()
    {
        $meting = factory(Meting::class)->create([
            'co2' => 400,
        ]);
        $this->assertTrue($meting->co2_good);

        $meting = factory(Meting::class)->create([
            'co2' => 900,
        ]);
        $this->assertFalse($meting->co2_good);
    }


    public function testTemperatureGoodAttribute()
    {
        $meting = factory(Meting::class)->create([
            'temperature' => 20,
        ]);
        $this->assertTrue($meting->temperature_good);

        $meting = factory(Meting::class)->create([
            'temperature' => 30,
        ]);
        $this->assertFalse($meting->temperature_good);
    }


    public function testHumidityGoodAttribute()
    {
        $meting = factory(Meting::class)->create([
            'humidity' => 55,
        ]);
        $this->assertTrue($meting->humidity_good);

        $meting = factory(Meting::class)->create([
            'humidity' => 20,
        ]);
        $this->assertFalse($meting->humidity_good);

        $meting = factory(Meting::class)->create([
            'humidity' => 75,
        ]);
        $this->assertFalse($meting->humidity_good);
    }



    public function testTimeSinceLastAttribute()
    {
        $meting = factory(Meting::class)->create([
            'created_at' => Carbon::now()->addHours(-2),
        ]);
        $this->assertEquals($meting->time_since_last, 120);



        $meting = factory(Meting::class)->create([
            'created_at' => Carbon::now()->addMinutes(-20),
        ]);
        $this->assertEquals($meting->time_since_last, 20);
    }



    public function testGetLatestByRoom()
    {
        $meting = factory(Meting::class)->create([
            'room' => 100
        ]);

        $latest_from_database = Meting::getLatestByRoom(100);

        $this->assertEquals($meting['attributes'], $latest_from_database['attributes']);
    }
}
