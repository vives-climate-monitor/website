<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Traits\WithDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithDatabase;

    protected function setUp(): void
    {
        $uses = array_flip(class_uses_recursive(static::class));

        parent::setUp();

        if (isset($uses[WithDatabase::class])) {
            $this->setUpDatabase();
        }
    }
}
