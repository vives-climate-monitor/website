<?php

namespace App;

use Carbon\Carbon;
use App\Traits\GroupedLastScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class Meting extends Model
{
    use GroupedLastScope;

    protected $fillable = [
        'room', 'co2', 'temperature', 'humidity', 'tvoc',
    ];

    /**
     * Het aantal minuten sinds de laatste meting.
     */
    public function getTimeSinceLastAttribute(): int
    {
        return Carbon::parse($this->created_at)->diffInMinutes(Carbon::now());
    }

    /**
     * Hoe goed de CO2 waarde is.
     */
    public function getCo2GoodAttribute(): bool
    {
        return $this->co2 < config('constants.meting.co2_max');
    }

    /**
     * Hoe goed de vochtigheid is.
     */
    public function getHumidityGoodAttribute(): bool
    {
        return $this->humidity >= Config::get('constants.meting.humidity_min') && $this->humidity <= Config::get('constants.meting.humidity_max');
    }

    /**
     * Hoe goed de temperatuur  is.
     */
    public function getTemperatureGoodAttribute(): bool
    {
        return $this->temperature >= Config::get('constants.meting.temperature_min') && $this->temperature <= Config::get('constants.meting.temperature_max');
    }

    public static function getLatestByRoom($room): Meting
    {
        return Meting::where('room', $room)->latest()->first();
    }

    public static function getLatestAverage(): Meting
    {
        return Cache::remember('latest_average', 300, function () {
            $temp = Meting::interval('Realtime')->latest()->first();
            $avg = new Meting((array) $temp);
            $avg->created_at = $temp->created_at;

            return $avg;
        });
    }

    /**
     * Laatste meting by room(s) per interval.
     *
     * @param $rooms Integer || Array(Integer)
     * @param $interval String
     * @param $options [$period [Timestring, Timesting], $format Boolean ]
     *
     * @return Meting or Meting[]
     */
    public static function getPerIntervalByRoom($rooms, $interval, $options = null)
    {
        if (is_array($rooms)) {
            $result = [];
            foreach ($rooms as $room) {
                $latest = self::getPerIntervalByRoom($room, $interval, $options);
                $result[] = ['room' => $room, 'metingen' => $latest->values()];
            }

            return $result;
        }

        $room = $rooms;

        if (! empty($options['period'][0])) {
            $period = $options['period'];
        } else {
            $period = Meting::getPeriod($interval);
        }
        $format = $options['format'] ?? false;

        $metingen = Meting::where('room', $room)
            ->whereBetween(
                'created_at',
                [Carbon::createFromTimeString($period[0]), Carbon::createFromTimeString($period[1])]
            )->interval($interval)->latest()->take(15)->get()->reverse();

        if ($format) {
            return Meting::formatTimeStamps($metingen, $interval);
        }

        return $metingen;
    }

    /**
     * Laatste agg meting per interval
     * agg => aggregate => min/max/average.
     *
     * @return Meting
     */
    public static function getAllPerInterval($interval, $agg = 'avg', $period = null)
    {
        $_period = $period;

        //indien geen period meegegeven, zelf bepalen
        if (! $period[0]) {
            $_period = Meting::getPeriod($interval);
        }

        return Cache::remember("all_per_interval:${interval}:${agg}:".$_period[0].':'.$_period[1], 300, function () use ($interval, $agg, $_period) {
            return Meting::whereBetween(
                'created_at',
                [Carbon::createFromTimeString($_period[0]), Carbon::createFromTimeString($_period[1])]
            )->interval($interval, $agg)->latest()->take(15)->get()->reverse();
        });
    }

    /**
     * get period when not provided.
     *
     * FORMAT : Thu Apr 09 2020 00:00:00
     */
    public static function getPeriod($interval): array
    {
        switch ($interval) {
            case 'Hourly':
                return [Carbon::now()->subDays(2), Carbon::now()];
            case 'Daily':
                return [Carbon::now()->subWeeks(2), Carbon::now()];
            case 'Weekly':
                return [Carbon::now()->subWeeks(15), Carbon::now()];
            case 'Monthly':
                return [Carbon::now()->subMonths(10), Carbon::now()];
            default:
                return [Carbon::now()->subHours(4), Carbon::now()];
        }
    }

    /**
     * format timestamps.
     */
    public static function formatTimeStamps($metings, $interval)
    {
        switch ($interval) {
            case 'Realtime':
                $metings = $metings->map(function ($m) {
                    $m->created_at = \Carbon\Carbon::parse($m->created_at)->format('d/m/y - H:i');

                    return $m;
                });
                break;
            case 'Hourly':
                $metings = $metings->map(function ($m) {
                    $m->created_at = \Carbon\Carbon::parse($m->created_at)->format('d/m/y - H');

                    return $m;
                });
                break;
            case 'Daily':
            case 'Weekly':
                $metings = $metings->map(function ($m) {
                    $m->created_at = \Carbon\Carbon::parse($m->created_at)->format('d/m/y');

                    return $m;
                });
                break;
            case 'Monthly':
                $metings = $metings->map(function ($m) {
                    $m->created_at = \Carbon\Carbon::parse($m->created_at)->format('m/y');

                    return $m;
                });
                break;
            default:
                break;
        }

        return $metings;
    }

    /**
     * Aantal minuten boven x van poperty y by room.
     *
     * @return object (duration, property, above)
     */
    public static function TimeAboveByRoom(string $room, $param, $value)
    {
        return Cache::remember("time_above:${room}:${param}:${value}", 300, function () use ($room, $param, $value) {
            /**
             * select min(created_at), max(created_at), count(*) as numrecs, avg(temperature)
             * from ( select room,created_at, temperature,
             *  max(case when temperature < 18 then created_at end) over (order by created_at rows unbounded preceding) as prevlow
             * from metings where room = 130) as dt
             * where temperature >= 18
             * group by prevlow.
             */
            $result = DB::select("
                select min(created_at) as minimum, max(created_at) as maximum, count(*) as numrecs, avg({$param}) as average
                from ( select room,created_at, {$param},
                max(case when {$param} < ? then created_at end)
                over (order by created_at rows unbounded preceding) as prevlow
                from `metings` where room = ?) as dt
                where {$param} >= ?
                group by prevlow
                ", [$value, $room, $value]);

            $duration['property'] = $param;
            $duration['duration'] = 0;
            $duration['above'] = $value;

            foreach ($result as $res) {
                $duration['duration'] += Carbon::createFromDate($res->minimum)->diffInHours($res->maximum);
            }

            return $duration;
        });
    }

    /**
     * Get alle lokaalnummers.
     */
    public static function getRooms(): array
    {
        return Cache::remember('rooms', 300, fn () => Meting::distinct()->orderBy('room')->get(['room'])->pluck('room')->toArray());
    }

    public function scopeInterval($query, $interval = 'Realtime', $agg = 'avg')
    {
        /*
         *  300         // 5 minutes
         *  86400 / 24  // hourly
         *  86400       //day
         *  7 * 86400   //week
         *  31 * 86400  //month
         */

        switch ($interval) {
            case 'Hourly':
                $interval_unix = 86400 / 24;
                break;
            case 'Daily':
                $interval_unix = 86400;
                break;
            case 'Weekly':
                $interval_unix = 7 * 86400;
                break;
            case 'Monthly':
                $interval_unix = 31 * 86400;
                break;
            default:
                $interval_unix = 300;
                break;
        }

        //Min/Max/Average
        switch ($agg) {
            case 'Min':
            case 'min':
                $mma = 'min';
                break;
            case 'Max':
            case 'max':
                $mma = 'max';
                break;
            default:
                $mma = 'avg';
                break;
        }

        //select avg(co2) as co2, avg(temperature) as temperature, count(*) as count, min(created_at) as created_at, floor(UNIX_TIMESTAMP(created_at) / 86400 ) as my_interval from metings where room=101 group by my_interval;

        return $query
            ->toBase()
            ->selectRaw(
                "{$mma}(co2) as co2,".
                    "{$mma}(temperature) as temperature,".
                    "{$mma}(humidity) as humidity,".
                    'count(*) as count, '.
                    'min(created_at) as created_at, '.
                    'floor(UNIX_TIMESTAMP(created_at) / ? ) as my_interval',
                [$interval_unix]
            )
            ->groupBy('my_interval');
    }
}
