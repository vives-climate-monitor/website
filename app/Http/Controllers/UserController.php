<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function AuthRouteAPI(Request $request)
    {
        return $request->user();
    }

    public function setLanguage($language): RedirectResponse
    {
        Session::put('language', $language);
        return Redirect::back();
    }
}
