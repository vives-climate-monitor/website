<?php

namespace App\Http\Controllers;

use App\Meting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class MetingController extends Controller
{
    public function __construct()
    {
        //route protection on create/delete/edit
        $this->middleware('auth', ['except' => ['index', 'room', 'average']]);
    }

    public function index()
    {
        $latest = Meting::lastPerGroup(['room'])->orderBy('room')->paginate(12);
        $count = Meting::count();

        return view('metings.index', ['metings' => $latest, 'count' => $count]);
    }

    public function list()
    {
        $metings = Meting::latest()->paginate(8);

        return view('metings.list', compact('metings'))
            ->with('i', (request()->input('page', 1) - 1) * 8);
    }

    public function create()
    {
        return view('metings.create');
    }

    public function store(Request $request)
    {
        Meting::create($request->all());

        return redirect()->route('metings.list')
            ->with('success', 'Meting created successfully.');
    }

    public function show(Meting $meting)
    {
        /** get last records for this room */
        $latest = Meting::where('room', $meting->room)->get();

        return view('metings.show', ['meting' => $meting, 'latest' => $latest]);
    }

    /**
     * Display all metings for certain room.
     */
    public static function room(Request $request, string $room)
    {
        /**latest record based on room */
        $meting = Meting::getLatestByRoom($room);

        if (empty($meting)) {
            /* geen meting gevonden met ID */
            return abort(404);
        }

        /* axios call vanuit ChartComponent (in browser) gaat hierop returnen
         *  => nieuwe data zonder view te reloaden
         */
        if (request()->wantsJson()) {
            $interval = request()->get('interval');

            $latest = Meting::getPerIntervalByRoom($meting->room, $interval, ['format' => true]);

            return response($latest->values());
        }

        /** count number of metings for this room */
        $count = Meting::where('room', $meting->room)->count();

        $timeAboveData[] = ['data' => Meting::TimeAboveByRoom($room, 'co2', 250)];
        $timeAboveData[] = ['data' => Meting::TimeAboveByRoom($room, 'temperature', 10)];
        $timeAboveData[] = ['data' => Meting::TimeAboveByRoom($room, 'humidity', 10)];

        return view(
            'metings.room',
            [
                'meting' => $meting,
                'count' => $count,
                'timeAboveData' => $timeAboveData,
            ]
        );
    }

    public static function average()
    {
        /**latest record based on room */
        $avg = Meting::getLatestAverage();

        if (empty($avg)) {
            return abort(404);
        }

        return view(
            'metings.average',
            [
                'meting' => $avg,
                'roomnumbers' => Meting::getRooms(),
            ]
        );
    }

    public function edit(Meting $meting)
    {
        return view('metings.edit', compact('meting'));
    }

    public function update(Request $request, Meting $meting): RedirectResponse
    {
        $request->validate([
            'room' => 'required',
            'co2' => 'required',
            'temperature' => 'required',
        ]);

        $meting->update($request->all());

        return redirect()->route('metings.list')
            ->with('success', 'Meting updated successfully.');
    }

    public function destroy(Meting $meting): RedirectResponse
    {
        $meting->delete();

        return redirect()->route('metings.list')
            ->with('success', 'Meting deleted successfully');
    }
}
