<?php

namespace App\Http\Controllers\API;

use App\Meting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeAboveController extends Controller
{
    public function invoke(Request $request): JsonResponse
    {
        return response()->json(
            Meting::TimeAboveByRoom(
                $request->get('room'),
                $request->get('property'),
                $request->get('value')
            )
        );
    }
}
