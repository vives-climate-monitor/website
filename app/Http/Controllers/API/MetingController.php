<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Meting;
use Illuminate\Http\JsonResponse;

class MetingController extends Controller
{
    public function store(): JsonResponse
    {
        $authorization = request()->header('authorization');

        if (empty($authorization)) {
            return response()->json(['message' => 'No api token provided']);
        }
        if ($authorization != env('SECRET_KEY')) {
            return response()->json(['message' => 'Invalid api token provided']);
        }

        request()->validate([
            'room' => 'required|integer|max:999|min:0',
            'co2' => 'required|integer|max:2000|min:10',
            'temperature' => 'required|integer|max:40|min:0',
            'humidity' => 'required|integer|max:100|min:0',
            'tvoc' => 'required|integer|max:2000|min:0',
        ]);

        Meting::create(request()->all());

        return response()
            ->json([
                'meting' => Meting::latest()->first(),
                'successful' => true,
            ], 201);
    }
}
