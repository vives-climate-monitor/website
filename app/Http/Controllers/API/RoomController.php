<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class RoomController extends Controller
{
    public function list(): JsonResponse
    {
        return response()->json(\App\Meting::getRooms());
    }
}
