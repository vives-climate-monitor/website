<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Meting;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class MetingAverageController extends Controller
{
    public function invoke(): JsonResponse
    {
        //dump(request()->all());

        $interval = request()->get('interval');
        $rooms = request()->get('rooms');
        $period = request()->get('period');

        //validate parameters
        $validator = Validator::make(request()->all(), [
            'format' => 'boolean',
            'period' => 'array',
            'period.0' => 'nullable|date',
            'period.1' => 'nullable|date',
        ]);

        if ($validator->fails()) {
            //pass validator errors as errors object for ajax response
            return response()->json(['errors' => $validator->errors()]);
        }

        //get latest metingen per interval of the selected rooms
        $result = Meting::getPerIntervalByRoom(
            $rooms,
            $interval,
            ['period' => $period, 'format' => false]
        );

        $avg = Meting::getAllPerInterval($interval, 'avg', $period);
        $result[] = ['room' => 'average', 'metingen' => $avg->values()];

        $min = Meting::getAllPerInterval($interval, 'min', $period);
        $result[] = ['room' => 'min', 'metingen' => $min->values()];

        $max = Meting::getAllPerInterval($interval, 'max', $period);
        $result[] = ['room' => 'max', 'metingen' => $max->values()];

        return response()->json($result);
    }
}
