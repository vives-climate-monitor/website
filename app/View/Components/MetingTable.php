<?php

namespace App\View\Components;

use App\Meting;
use Illuminate\View\Component;

class MetingTable extends Component
{
    public $meting;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Meting $meting)
    {
        $this->meting = $meting;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.meting-table');
    }
}
