<?php
$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__);

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        '@PSR2' => true,
        'yoda_style' => false,
        'array_indentation' => true,
        'method_argument_space' => [
            'on_multiline' => 'ensure_fully_multiline'
        ],
        'method_chaining_indentation' => true,
        'array_syntax' => [
            'syntax' => 'short',
        ],
        'not_operator_with_successor_space' => true,
        'binary_operator_spaces' => [
            'operators' => [
                '=' => 'single_space',
                '=>' => 'single_space',
            ],
        ],
        'ordered_imports' => [],
        'no_unused_imports' => true,
        'implode_call' => true,
        'no_alias_functions' => true,
        'no_alias_functions' => true,
        'use_arrow_functions' => true,
        'modernize_types_casting' => true,
        'no_superfluous_phpdoc_tags' => [
            'allow_mixed' => true,
            'remove_inheritdoc' => true
        ],
        'single_blank_line_at_eof' =>true
    ])
    ->setRiskyAllowed(true)
    ->setIndent('    ')
    ->setCacheFile(__DIR__.'/.php-cs.cache')
    ->setFinder($finder);
