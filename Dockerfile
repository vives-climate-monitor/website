FROM php:8.0-apache
RUN apt-get update -y && apt-get install -y openssl libonig-dev zip unzip \
git nodejs curl nano libicu-dev libmcrypt-dev libzip-dev \
&& rm -r /var/lib/apt/lists/* \
&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
&& docker-php-ext-install pdo pdo_mysql

#set our application folder as an environment variable
ENV APP_HOME /var/www/html

#change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

#change the web_root to laravel /var/www/html/public folder
RUN sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf

# enable apache module rewrite
RUN a2enmod rewrite

#copy source files and run composer
WORKDIR /var/www/html
COPY . /var/www/html
RUN composer install --no-dev --no-interaction --optimize-autoloader
RUN curl https://www.npmjs.com/install.sh | sh \
&& npm install --global cross-env
RUN npm i && npm run prod && rm -r node_modules/ \
&& php artisan route:cache

#change ownership of our applications
RUN chown -R www-data:www-data $APP_HOME
